<?php

namespace Mnm\MnmQuizBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Doctrine\ORM\NoResultException;

class Controller extends BaseController {

    /**
     * @return \Symfony\Component\Security\Core\SecurityContext
     */
    protected function getSecurityContext() {
        return $this->container->get('security.context');
    }

    /**
     * @return \Mnm\MnmUserBundle\Entity\User
     */
    public function getUser() {
        return parent::getUser();
    }

    public function getUserCrn() {
        $em = $this->getDoctrine()->getManager();
        $userCrn = $em->getRepository('MnmBluestemBundle:AuthorizedUsers')
                ->findOneBy(
                array(
                    "username" => $this->getUser()->getUsername(),
                    "isAuthorized" => 1
                )
        );

        if ($userCrn) {
            if ($userCrn->getCrn() != "") {

                return $userCrn->getCrn();
            }
        }
        //This is the CRN for Wed Lab
        return 25963;
    }

    public function getUserHwDue() {
        $weekdays = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday");
        $em = $this->getDoctrine()->getManager();
        $usrCrn = $em->getRepository('HomeBundle:CrnList')
          ->findOneByCrn($this->getUserCrn());
//        print_r($this->getUserCrn());die;
        $weekday = $weekdays[$usrCrn->getWeekday()];

// print_r($usrCrn->getStartTime()-> format('H:i:s') );die;
        //unix timestamps
        $thisWeekDueTime = date('F j, Y H:i', strtotime($weekday . " this week " . $usrCrn->getStartTime()->format('H:i:s')));

        $nextWeekDueTime = date('F j, Y H:i', strtotime($weekday . " next week " . $usrCrn->getStartTime()->format('H:i:s')));
        $nextWeekString = $weekday . " next week " . $usrCrn->getStartTime()->format('H:i:s');
        $now = date('F j, Y H:i', strtotime('now'));

//                print_r( date('F jS, Y h:i', $thisWeekDueTime)  );die;
//        $query = $em->createQuery(
//                'SELECT s.endDate, p.hwDue
//                            FROM HomeBundle:Assignment p
//                            JOIN HomeBundle:Syllabus s
//                            WHERE s.endDate > :date                            
//                            AND p.type=4
//                            AND  p.hwDue IS NOT NULL
//                            ORDER BY s.endDate ASC'
//        );
        $repository = $em->getRepository('HomeBundle:Syllabus');
        $query = $repository->createQueryBuilder('s')
                ->select('s.endDate', 'p.hwDue')
                ->join('HomeBundle:Assignment', 'p', 'WITH', 'p.week = s.id')
                ->where('s.endDate > :date AND p.hwDue IS NOT NULL');
        ;
        $query->setMaxResults(1);
        if (strtotime('now') < strtotime($weekday . " this week " . $usrCrn->getStartTime()->format('H:i:s'))) {
            $query->setParameter('date', new \DateTime("now"));
            $dueDateTime = $thisWeekDueTime;
        } else {
            $query->setParameter('date', new \DateTime($nextWeekString));
            $dueDateTime = $nextWeekDueTime;
        }
        try {
            $result = $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            $result["hwDue"] = 0;
        } catch (Exception $e) {
            return null;
        }


//        print_r($result);die;
        return array('dueDateTime' => $dueDateTime,
            'quizDue' => $result["hwDue"]
        );
    }

    /**
     * 
     * @param type $collection
     * @return type mongodb collection
     */
    public function getMongoCollection($collection) {
        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $mongoDatabase = $this->container->getParameter('mongodb_database');
        $db = $m->selectDatabase($mongoDatabase);
        return $db->$collection;
    }

    /**
     * 
     * @param type $criteria
     * @param type $query
     * @param type $collection
     * @return type array()
     */
    public function getMongoData($criteria, $query, $collection, $sort) {
        $cursor = $collection->find($criteria, $query)->sort($sort)->limit(1);
        return iterator_to_array($cursor);
    }

}

?>
