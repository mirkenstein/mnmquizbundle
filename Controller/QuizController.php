<?php

namespace Mnm\MnmQuizBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class QuizController extends Controller {

    /**
     * @Route("/",name="mnm_quiz_index") 
     * @Template()
     */
    public function indexAction() {
        return $hwDue = $this->getUserHwDue();
//        print_r($hwDue);die;
//        $crn = array('quizDue' => 1, 'dueDateTime' => date('F j, Y H:i', strtotime("Friday" . " next week " . "14:00")));
//        return $crn;
    }

    /**
     * @Route("/get",name="mnm_quiz_get") 
     */
    public function getAction(Request $request) {
        $postedData = json_decode($request->getContent(), true);
        $quiz = $postedData["quiz"];

        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $mongoDatabase = $this->container->getParameter('mongodb_database');
        $db = $m->selectDatabase($mongoDatabase);

        $collection = $db->quizData;
        $query = array('extra' => 0, '_id' => 0, "Quiz" => 0);
        $cursor = $collection->find(array("Quiz" => $quiz), $query);
        $response = new JsonResponse();
        $response->setData(
                iterator_to_array($cursor)[0]
        );
        return $response;
    }

    /**
     * @Route("/answer",name="mnm_quiz_answer") 
     */
    public function answerAction(Request $request) {
        $postedData = json_decode($request->getContent(), true);
        $quiz = $postedData["quiz"];

        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $mongoDatabase = $this->container->getParameter('mongodb_database');
        $db = $m->selectDatabase($mongoDatabase);

        $collection = $db->quizAnswers;
        $postedData = $request->getContent();
        //get the logged in user
        $user = $this->getUser();
        $answer = json_decode($postedData, true);

        //Get the solutions array from mong
        $solutions = $db->quizSolutions;
        $query = array('extra' => 0, '_id' => 0, "Quiz" => 0);
        $cursor = $solutions->find(array("Quiz" => $quiz, "NUMBER" => $answer['q']), $query);
        $sol = iterator_to_array($cursor)[0];
        //Evaluating wheather the answer is correct
        $isCorrect = 0;
        if ($sol['ANS'] == $answer['ans']) {
            $isCorrect = 1;
        } elseif (is_numeric($sol['ANS']) && round($sol['ANS'], 1) == round($answer['ans'], 1)) {
            $isCorrect = 1;
        };
        $answer = array_merge($answer, array("isCorrect" => $isCorrect, "created_at" => date("Y-m-d H:i:s")));
//        print_r($sol);
        $collection->update(
                array('user' => $user->getUsername(), "Quiz" => $quiz, "q" => $answer['q']), array('$set' => $answer), array('upsert' => true)
        );

        $response = new JsonResponse("OK");
        return $response;
    }

    /**
     * @Route("/safety",name="mnm_quiz_safety") 
     * @Template()
     */
    public function safetyAction() {
        $username = $this->getUser()->getUsername();
        $collection = $this->getMongoCollection('radSafetyData');
        $query = array('_id' => 0, 'username' => 1);
        $criteria = array(
            "username" => $username
//            "table" => array('$ne' => "Roster")
        );
        $sort = array();
        $data = $this->getMongoData($criteria, $query, $collection, $sort);
        if (count($data) > 0) {
            $isSubmitted = 1;
        } else {
            $isSubmitted = 0;
        }

        return array('isSubmitted' => $isSubmitted);
//        print_r($hwDue);die;
//        $crn = array('quizDue' => 1, 'dueDateTime' => date('F j, Y H:i', strtotime("Friday" . " next week " . "14:00")));
//        return $crn;
    }

    /**
     * @Route("/submit",name="mnm_quiz_submit") 
     */
    public function submitAction(Request $request) {
        $postedData = json_decode($request->getContent(), true);
        $classNr = $postedData["classNr"];
        $user = $this->getUser();
        $username = $user->getUsername();
        $firstName = $user->getFirstName();
        $lastName = $user->getLastName();
        $uin = $user->getUiEduUin();
//        $uin = $user->getUin();
        $collection = $this->getMongoCollection('radSafetyData');

        $item["user"]["username"] = $username; //tmp fix better work with the outer username 
        $item["class_number"] = $classNr;
        $item["timestamp"] = date('Y-m-d H:i:s', time());

        $collection->update(array(
            "username" => $username,
            "first_name" => $firstName,
            "last_name" => $lastName,
            "uin" => $uin
                ), array('$set' => $item), array('upsert' => true));

        return new JsonResponse(array("result" => "ok"));

        $response = new JsonResponse("OK");
        return $response;
    }

}
