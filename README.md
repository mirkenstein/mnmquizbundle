Mnm Quiz Bundle
========================

This document contains information on how to setup this  bundle.

1) Install as git submodule in you project src dir
------------------------------------------------------
From the root of your project add the submodule.  

           git submodule add git@bitbucket.org:mirkenstein/mnmquizbundle.git  ./src/Mnm/MnmQuizBundle
           git submodule add https://mirkenstein@bitbucket.org/mirkenstein/mnmquizbundle.git ./src/Mnm/MnmQuizBundle

2) Add in your project `composer.json` these two bundles
--------------------------------------------
    "require": {
            //...
        "doctrine/mongodb-odm": "1.0.*@dev",
        "doctrine/mongodb-odm-bundle": "3.0.*@dev"
          }

3) Register the bundles to your application kernel
--------------------------------------------
        // app/AppKernel.php
        public function registerBundles()
        {
            return array(
                // ...
                new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
                new Mnm\MnmQuizBundle\MnmGradingBundle(),
                // ...
            );
        }


3) Update your routing 
--------------------------------------------
      //app/config/routing.yml
        mnm_grading:
            resource: "@MnmQuizBundle/Controller"
            prefix:   /quiz
            type: annotation  
     


4) Add in your main config.yml
--------------------------------------------
		doctrine_mongodb:
			connections:
				default:
					server: mongodb://10.0.10.1:27017
					options: {}
			default_database: test_database
			document_managers:
				default:
					auto_mapping: true   




 db.quizData.ensureIndex({"Quiz":1},{unique: true})
mongoimport -h  10.0.10.1:27017 --db testData --collection quizData <quiz1ab.min.json