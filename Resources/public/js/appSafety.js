//Questions Data
var questionsData = {
    "Quiz": "RadiationSafety",
    "data": [
        {
            "TOPIC": "Radiation Safety Test",
            "QUESTIONS": [
                {
                    "NUMBER": 1,
                    "QUESTION": "No eating, drinking, or applying of cosmetics is allowed in a laboratory using radiation, except in designated areas and after washing one's hands?",
                    "ANSWERS": {
                        "A": "True",
                        "B": "False"
                    }
                },
                {
                    "NUMBER": 2,
                    "QUESTION": "Only hold edges of disk. Avoid touching the unlabeled flat side of disk",
                    "ANSWERS": {
                        "A": "True",
                        "B": "False"
                    }
                },
                {
                    "NUMBER": 3,
                    "QUESTION": "Never touch or tape the active area of foil source",
                    "ANSWERS": {
                        "A": "True",
                        "B": "False"
                    }
                },
                {
                    "NUMBER": 4,
                    "QUESTION": "What is the annual radiation dose to the average person living in the United States?",
                    "ANSWERS": {
                        "A": "2000 mrem",
                        "B": "620 mrem",
                        "C": "360 mrem",
                        "D": "30 mrem"
                    }
                },
                {
                    "NUMBER": 5,
                    "QUESTION": " A sample of Thallium-204 has 10 mCi of activity. The Curie (Ci) is one unit for measurement of:",
                    "ANSWERS": {
                        "A": "The ability of photons to produce ionizing radiation",
                        "B": "Rate of radioactive events (eg. disintegrations per second)",
                        "C": "The amount of energy absorbed by tissue",
                        "D": "All of the above"
                    }
                },
                {
                    "NUMBER": 6,
                    "QUESTION": "The most important factor for determining the exposure hazard of a particular isotope is:",
                    "ANSWERS": {
                        "A": "Activity",
                        "B": "Decay energy",
                        "C": "Half-value layer",
                        "D": "Physical state"
                    }
                },
                {
                    "NUMBER": 7,
                    "QUESTION": "Which type of external radiation would be considered the most hazardous, given an equal amount of activity and distance from the source?",
                    "ANSWERS": {
                        "A": "An alpha source",
                        "B": "A beta source",
                        "C": "A gamma source",
                        "D": "All are equally hazardous"
                    }
                },
                {
                    "NUMBER": 8,
                    "QUESTION": "Which radiation is least penetrating of human skin tissue?",
                    "ANSWERS": {
                        "A": "Beta particle",
                        "B": "Alpha particle",
                        "C": "Gamma ray",
                        "D": "Xray"
                    }
                }, {
                    "NUMBER": 9,
                    "QUESTION": "Beta particles are ...",
                    "ANSWERS": {
                        "A": "emitted from the nucleus with discrete energies",
                        "B": "capable of creating bremsstrahlung radiation in materials",
                        "C": "essentially identical to a proton",
                        "D": "all of the above"
                    }
                }, {
                    "NUMBER": 10,
                    "QUESTION": "Which of the following would be the most effective shielding material?",
                    "ANSWERS": {
                        "A": "Wood",
                        "B": "Paper",
                        "C": "Scissors",
                        "D": "Concrete"
                    }
                }

            ]
        }

    ]
};

//End Questions Data
var app = angular.module('quiz', ['ngAnimate', 'xeditable']);
app.run(function (editableOptions) {
    editableOptions.theme = 'bs3';
});
app.factory('Item', function ($http) {
    var Item = function () {
        return initData;
    }
    Item.doit = function (item) {
        $http.post('../ajax/post', item).success(function (data) {
            Item = true;
        }).error(function () {
            Item = false;
        });

    }
    return Item;
});
var qa = ["", "A", "A", "A", "B", "B", "A", "C", "B", "A", "D"];
app.controller('QuizCtrl', ['$scope', 'Item', '$http', function ($scope, Item, $http) {
        $scope.infomessage = "You must answer all questions correctly in order to pass the test";
        $scope.submitted = isSubmitted;
        if ($scope.submitted) {
            $scope.infomessage = "You have already completed this test. No need to retake it.";
        }
        $scope.pts = 0;
        $scope.quiz = quizNr;
        $scope.items = questionsData.data;
//        $http.post('get', './radSafety.json').success(function (data) {
//            console.log("done");
//            $scope.items = data.data;
//            $scope.got = true;
//        });

        $scope.ans = [];

        $scope.select = function (ques, ans) {
//            $http.post('answer', {q: ques, ans: ans, quiz: $scope.quiz});
            if (ans === qa[ques]) {
//                console.log("YES");
                $scope.ans[ques] = 1;
            } else {
                $scope.ans[ques] = 0;
//                console.log("not correct" + $scope.pts);
            }
        }
        $scope.textans = function (ques, ans) {
            $http.post('answer', {q: ques, ans: ans, quiz: $scope.quiz})
        }
//        $scope.checkAnswer = function(data) {
//            if (parseFloat(data) == "NaN") {
//                return "Answer should be numeric";
//            }
//        };

        $scope.submitquiz = function () {
            var res = 0;
//calculate all the points 
            $scope.ans.map(function (val, key) {
                res += val;
            })
            if (res == 10) {
                $scope.infomessage = "You have succesffully completed the radiation safety test.";
                $scope.submitted = true;
                $http.post('submit', {classNr: classNr});
            } else {
                alert(" You did not answer correctly to " + (10 - res) + " questions. Please try again.")
            }


        }

        $scope.remaining = function () {
            return $scope.items.reduce(function (count, item) {
                return item.done ? count : count + 1;
            }, 0);
        };



        $scope.remove = function (item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
            $http.post('../ajax/remove', item);
        };

        $scope.submit = function () {
            $http.post('../ajax/submit', {vendor: $scope.vendor}).success(function (data) {
                if (data.status == "OK") {
                    $scope.submitted = true;
                }
            });
        };


    }]);
