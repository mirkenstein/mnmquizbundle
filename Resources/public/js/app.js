var app = angular.module('quiz', ['ngAnimate', 'xeditable']);
app.run(function(editableOptions) {
    editableOptions.theme = 'bs3';
});
app.factory('Item', function($http) {
    var Item = function() {
        return initData;
    }
    Item.doit = function(item) {
        $http.post('../ajax/post', item).success(function(data) {
            Item = true;
        }).error(function() {
            Item = false;
        });

    }
    return Item;
});

app.controller('QuizCtrl', ['$scope', 'Item', '$http', function($scope, Item, $http) {
//        var winLocArr = window.location.pathname.split("/");
//        $scope.vendor = winLocArr[winLocArr.length - 1];
        $scope.infomessage = "You can submit the homework multiple times before the deadline but only the latest submission will be graded.";
        $scope.submitted = false;
        $scope.quiz = quizNr;
        $scope.getquiz = function() {
            $http.post('get', {quiz: $scope.quiz}).success(function(data) {
                $scope.items = data.data;
                $scope.got = true;
            });

        }

        $scope.select = function(ques, ans) {
            $http.post('answer', {q: ques, ans: ans, quiz: $scope.quiz});

        }
        $scope.textans = function(ques, ans) {
            $http.post('answer', {q: ques, ans: ans, quiz: $scope.quiz})
        }
//        $scope.checkAnswer = function(data) {
//            if (parseFloat(data) == "NaN") {
//                return "Answer should be numeric";
//            }
//        };

        $scope.submitquiz = function() {
//            $scope.got = false;
//            $scope.items = [];
            $scope.infomessage = "Answers received. Thank you for your submission.";
            $scope.submitted = true;

        }

        $scope.remaining = function() {
            return $scope.items.reduce(function(count, item) {
                return item.done ? count : count + 1;
            }, 0);
        };



        $scope.remove = function(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
            $http.post('../ajax/remove', item);
        };

        $scope.submit = function() {
            $http.post('../ajax/submit', {vendor: $scope.vendor}).success(function(data) {
                if (data.status == "OK") {
                    $scope.submitted = true;
                }
            });
        };


    }]);
